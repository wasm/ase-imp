# godot .ase importer

an addon that lets you import aseprite files directly into the godot game engine as textures or scenes

at the moment it's pretty fragile, but at least parses everything i throw at it.

has initial support for automatically setting up
- animations (from tags)
- collision shapes (from slices)

mostly uploading as a (hopefully) useful reference

### Example project
- `Project > Project Settings... > Plugins > Enable` to enable the addon
- expand the "ase" folder, select the "example_slices.ase" file, and in the Import panel change "Import As:" from "Aseprite Texture" to "Aseprite
- Restart godot (godot'll warn and do this for you)
- Add "example_slices.ase" to a scene, and select its AnimationPlayer node
- Press play
