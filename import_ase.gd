tool
extends EditorImportPlugin

# TODO: make the code readable

# aseprite file format specification
# https://github.com/aseprite/aseprite/blob/master/docs/ase-file-specs.md

func read_ase(source_file) -> Dictionary:
	var file = File.new()
	var err = file.open(source_file, file.READ)
	if err != OK:
		return err
	var ase = {}
	ase.header = read_header(file)
	ase.frames = []
	ase.layers = []
	ase.slices = []
	ase.cels = []
	var startpos = 128 # length of file header
	for i in range(ase.header.frames):
		file.seek(startpos)
		ase.cels.append([])
		var chunkpos = startpos + 16 # length of frame header
		startpos += file.get_32()
		scroll(file, 4)
		ase.frames.push_back(file.get_16())
		scroll(file, 2)
		var chunks = file.get_32()
		for j in range(chunks):
			file.seek(chunkpos)
			var size = file.get_32()
			var type = "0x%04x" % file.get_16()
			chunkpos += size
			match type:
				"0x2004": ase.layers.push_back(read_layer(file))
				"0x2005": ase.cels[i].append(read_cel(file, size, ase.header))
				"0x2018": ase.tags = read_tags(file)
				"0x2019": ase.header.palette = read_palette(file)
				"0x2022": ase.slices.push_back(read_slices(file))
				_: pass
	ase.layers = layer_postprocess(ase.layers)
	if len(ase.slices)>0:
		ase.slices = slices_postprocess(ase)
	file.close()
	return ase

func read_header(file) -> Dictionary:
	var header = {}
	var filesize = file.get_32()
	file.seek(6)
	header.frames = file.get_16()
	header.width = file.get_16()
	header.height = file.get_16()
	header.colordepth = file.get_16()
	header.flags = file.get_32()
	scroll(file, 10) # deprecated and empty bytes
	header.alpha = file.get_8()
	scroll(file, 3) # spec says: ignore these bytes
	header.colors = file.get_16()
	header.pixelaspect = [file.get_8(), file.get_8()]
	# skip reading grid attributes
	return header

func read_layer(file) -> Dictionary:
	var layer = {}
	layer.flags = file.get_16()
	layer.layertype = file.get_16()
	layer.level = file.get_16()
	scroll(file, 4)
	layer.blendmode = file.get_16()
	layer.opacity = file.get_8()
	scroll(file, 3)
	layer.name = get_str(file)
	return layer

func layer_postprocess(layers) -> Dictionary:
	var lastvisiblelevel = -1
	var layerinfo = []
	# navigate the layer tree and bake info to flat array
	for layer in layers:
		# check NOTE.1 and NOTE.2 in the spec
		var visible = bool(layer.flags & 1)
		if layer.level==0:
			lastvisiblelevel = layer.level if visible else -1
		elif visible and lastvisiblelevel+1>=layer.level:
			lastvisiblelevel = layer.level
		else:
			visible = false
		layerinfo.append({
			"blendmode":layer.blendmode,
			"name":layer.name,
			"visible":visible,
			"opacity":layer.opacity
		})
	return layerinfo

func read_cel(file, size, header) -> Dictionary:
	var cel = {}
	cel.layer = file.get_16()
	cel.posx = get_signed(file.get_16())
	cel.posy = get_signed(file.get_16())
	cel.opacity = file.get_8()
	cel.type = file.get_16()
	scroll(file, 7)
	match cel.type:
		0: # raw cel
			cel.width = file.get_16()
			cel.height = file.get_16()
			cel.buffer = file.getbuffer(size - 20)
		1: # linked cel
			# https://github.com/aseprite/aseprite/issues/2470
			cel.link = file.get_16()
		2: # compressed cel
			cel.width = file.get_16()
			cel.height = file.get_16()
			var buffer = file.get_buffer(size)
			cel.buffer = buffer.decompress(2147483647, 1) #max int
		_: pass
	if not cel.type==1:
		match header.colordepth:
			8: # indexed color mode
				var rgbabuffer = PoolByteArray()
				for i in range(len(cel.buffer)):
					var color = header.palette[cel.buffer[i]]
					rgbabuffer.append_array([
						color.r,
						color.g,
						color.b,
						color.a
					])
				cel.buffer = rgbabuffer
			16: # monochrome color mode
				var rgbabuffer = PoolByteArray()
				for i in range(len(cel.buffer) / 2):
					var color = {
						"bw":cel.buffer[i * 2],
						"a":cel.buffer[i * 2 + 1]
					}
					rgbabuffer.append_array([
						color.bw,
						color.bw,
						color.bw,
						color.a
					])
				cel.buffer = rgbabuffer
			32: pass # rgba color mode
	return cel

func read_tags(file) -> Array:
	var tags = []
	var tagnum = file.get_16()
	scroll(file, 8)
	for i in range(tagnum):
		tags.push_back( {} )
		tags[i].from = file.get_16()
		tags[i].to = file.get_16()
		tags[i].loop = file.get_8()
		scroll(file, 12)
		tags[i].name = get_str(file)
	return tags

func read_palette(file) -> Dictionary:
	var palette = {}
	var colors = file.get_32()
	var indexfirst = file.get_32()
	var indexlast = file.get_32()
	scroll(file, 8)
	palette = []
	for i in range(colors):
		palette.push_back( {} )
		var flags = file.get_16()
		palette[i].r = file.get_8()
		palette[i].g = file.get_8()
		palette[i].b = file.get_8()
		palette[i].a = file.get_8()
		if flags & 1:
			palette[i].name = get_str(file)
	return palette

func read_slices(file) -> Dictionary:
	var slices = {}
	var keyframes = file.get_32()
	var flags = file.get_32()
	scroll(file, 4)
	slices.name = get_str(file)
	slices.keys = []
	for i in range(keyframes):
		slices.keys.push_back( {} )
		slices.keys[i].frame = file.get_32()
		slices.keys[i].posx = get_signed(file.get_32())
		slices.keys[i].posy = get_signed(file.get_32())
		slices.keys[i].width = file.get_32()
		slices.keys[i].height = file.get_32()
		if flags & 1: # 9-patch
			var center = {}
			center.x = get_signed(file.get_32())
			center.y = get_signed(file.get_32())
			center.width = file.get_32()
			center.height = file.get_32()
			slices.keys[i].center = center
		if flags & 2: # pivot
			var pivot = {}
			pivot.x = get_signed(file.get_32())
			pivot.y = get_signed(file.get_32())
			slices.keys[i].pivot = pivot
	return slices

func slices_postprocess(ase) -> Array:
	var flatslices = []
	for slice in ase.slices:
		var keys = []
		for y in ase.header.frames:
			var oldkey = slice.keys[0].duplicate(true)
			for nkey in slice.keys:
				var newkey = nkey.duplicate(true)
				if newkey.frame>oldkey.frame and y>=newkey.frame:
					oldkey = newkey
			oldkey.frame = y
			keys.append(oldkey)
		var bslice = slice.duplicate(true)
		bslice.keys = keys
		flatslices.append(bslice)
	return flatslices

func build_frame_data(ase, framenum) -> Image:
	var img = Image.new()
	img.create(ase.header.width, ase.header.height, false, Image.FORMAT_RGBA8)
	var frame = ase.cels[framenum]
	for cel in frame:
		if ase.layers[cel.layer].visible:
			var alpha = cel.opacity
			if cel.has('link'):
				print(alpha)
				for link in ase.cels[cel.link]:
					if link.layer == cel.layer:
						cel = link
						alpha = 255
			elif ase.header.flags & 1:
				alpha *= ase.layers[cel.layer].opacity/255.0
			if alpha<255:
				cel.buffer = apply_alpha(cel.buffer, alpha)
			var dstrect = Rect2(0,0,cel.width, cel.height)
			var dstpos = Vector2(cel.posx,cel.posy)
			var dstimg = Image.new()
			dstimg.create_from_data(
				cel.width,
				cel.height,
				false,
				Image.FORMAT_RGBA8,
				cel.buffer
			)
			 # early out if blend mode is set to 'normal'
			if ase.layers[cel.layer].blendmode!=0:
				dstimg.data.data = blend_layers(
					img,
					dstimg,
					dstrect,
					dstpos,
					ase.layers[cel.layer].blendmode
				)
			img.blend_rect(dstimg, dstrect, dstpos)
			# NOTE: layer opacity is not cumulative
	return img

func build_atlas(ase) -> Image:
	var atlas = Image.new()
	# get squarest 2d tile atlas dimensions
	# - ignores frame dimensions, only looking at number of frames
	var dim = {"x": ceil(sqrt(ase.header.frames))}
	dim.y = dim.x if (ase.header.frames > dim.x * dim.x - dim.x) else dim.x-1
	atlas.create(
		ase.header.width * dim.x,
		ase.header.height*dim.y,
		false,
		Image.FORMAT_RGBA8
	)
	var canvasrect = Rect2(0, 0, ase.header.width, ase.header.height)
	for n in range(ase.header.frames):
		var frameimg = build_frame_data(ase, n)
		var framepos = Vector2(
			(n % int(dim.x)) * ase.header.width,
			ase.header.height * (n / int(dim.x))
		)
		atlas.blit_rect(frameimg, canvasrect, framepos)
	return atlas
	
func build_animator(ase, sprite):
	var animator
	if ase.header.frames>1:
		animator = build_animations(ase)
		if animator:
			sprite.add_child(animator)
			animator.owner = sprite
		animator = build_animated_colliders(ase, sprite, animator)

func build_animations(ase) -> AnimationPlayer:
	var animator = AnimationPlayer.new()
	if !ase.has("tags"):
		return null
	for tag in range(len(ase.tags)):
		var anim = Animation.new()
		var track_index = anim.add_track(Animation.TYPE_VALUE)
		anim.track_set_path(track_index, @".:frame")
		anim.value_track_set_update_mode(track_index, Animation.UPDATE_DISCRETE)
		anim.track_set_interpolation_type(track_index, Animation.INTERPOLATION_NEAREST)
		anim.set_loop(true)
		var curtime = 0
		for q in range(ase.tags[tag].to - ase.tags[tag].from + 1):
			var curframe = q + ase.tags[tag].from
			anim.track_insert_key(track_index, curtime, curframe)
			curtime += ase.frames[curframe] * 0.001
		animator.add_animation(ase.tags[tag].name, anim)
		anim.set_length(curtime)
	return animator

func build_animated_colliders(ase,sprite,animator) -> AnimationPlayer:
	for slice in ase.slices:
		var body = build_body(slice, sprite)
		var collider = CollisionShape2D.new()
		collider.shape = RectangleShape2D.new()
		collider.shape.extents = Vector2(slice.keys[0].width * 0.5, slice.keys[0].height * 0.5)
		collider.position.x = collider.shape.extents.x + slice.keys[0].posx - 0.5 * ase.header.width
		collider.position.y = collider.shape.extents.y + slice.keys[0].posy - 0.5 * ase.header.height
		body.add_child(collider)
		collider.owner = sprite
		var animations = animator.get_animation_list()
		for aname in animations:
			var anim = animator.get_animation(aname)
			var curtime = 0
			var tag
			for t in ase.tags:
				if t.name == aname:
					tag = t
			var track_shape_index = anim.add_track(Animation.TYPE_VALUE)
			var track_pos_index = anim.add_track(Animation.TYPE_VALUE)
			anim.track_set_path(track_shape_index, NodePath( str(sprite.get_path_to(collider)) + ":shape"))
			anim.track_set_path(track_pos_index, NodePath( str(sprite.get_path_to(collider)) + ":position"))
			anim.value_track_set_update_mode(track_shape_index, Animation.UPDATE_DISCRETE)
			anim.value_track_set_update_mode(track_pos_index, Animation.UPDATE_DISCRETE)
			anim.track_set_interpolation_type(track_shape_index, Animation.INTERPOLATION_NEAREST)
			anim.track_set_interpolation_type(track_pos_index, Animation.INTERPOLATION_NEAREST)
			anim.set_loop(true)
			for q in range(tag.to - tag.from + 1):
				var curframe = q + tag.from
				var curshape = RectangleShape2D.new()
				curshape.extents = Vector2(slice.keys[curframe].width*0.5, slice.keys[curframe].height*0.5)
				var curpos = Vector2(0,0)
				curpos.x = curshape.extents.x + slice.keys[curframe].posx - 0.5 * ase.header.width
				curpos.y = curshape.extents.y + slice.keys[curframe].posy - 0.5 * ase.header.height
				anim.track_insert_key(track_shape_index, curtime, curshape)
				anim.track_insert_key(track_pos_index, curtime, curpos)
				curtime += ase.frames[curframe] * 0.001
	return animator


func build_body(slice, sprite) -> Area2D:
	var body
	if sprite.has_node(slice.name):
		body = sprite.get_node(slice.name)
	else:
		body = Area2D.new()
		body.set_name(slice.name)
		sprite.add_child(body)
		body.owner = sprite
	return body
	
func apply_alpha(buffer, alpha) -> PoolByteArray:
	for i in range(len(buffer)/4):
		buffer[i*4+3] *= alpha/255.0
	return buffer

# blend in rgb only, retain fg.a and let blend_rect() deal with alpha
func blend_layers(src, dst, rect, pos, blendmode) -> PoolByteArray:
	dst.lock()
	src.lock()
	for x in range(rect.size.x):
		for y in range(rect.size.y):
			var bg = src.get_pixel(pos.x + x, pos.y + y)
			var fg = dst.get_pixel(rect.position.x + x, rect.position.y + y)
			var result = fg
			match blendmode:
				0:  # Normal
					result.r = fg.r
					result.g = fg.g
					result.b = fg.b
				1:  # Multiply
					result.r = bm_multiply(bg.r,fg.r)
					result.g = bm_multiply(bg.g, fg.g)
					result.b = bm_multiply(bg.b, fg.b)
				2:  # Screen
					result.r = bm_screen(bg.r, fg.r)
					result.g = bm_screen(bg.g, fg.g)
					result.b = bm_screen(bg.b, fg.b)
				3:  # Overlay
					result.r = bm_overlay(bg.r, fg.r)
					result.g = bm_overlay(bg.g, fg.b)
					result.b = bm_overlay(bg.b, fg.b)
				4:  # Darken
					result.r = bm_darken(bg.r, fg.r)
					result.g = bm_darken(bg.g, fg.b)
					result.b = bm_darken(bg.b, fg.b)
				5:  # Lighten
					result.r = bm_lighten(bg.r, fg.r)
					result.g = bm_lighten(bg.g, fg.b)
					result.b = bm_lighten(bg.b, fg.b)
				6:  # Color Dodge
					result.r = bm_colordodge(bg.r, fg.r)
					result.g = bm_colordodge(bg.g, fg.b)
					result.b = bm_colordodge(bg.b, fg.b)
				7:  # Color Burn
					result.r = bm_colorburn(bg.r, fg.r)
					result.g = bm_colorburn(bg.g, fg.b)
					result.b = bm_colorburn(bg.b, fg.b)
				8:  # Hard Light
					result.r = bm_hardlight(bg.r, fg.r)
					result.g = bm_hardlight(bg.g, fg.b)
					result.b = bm_hardlight(bg.b, fg.b)
				9:  # Soft Light
					result.r = bm_softlight(bg.r, fg.r)
					result.g = bm_softlight(bg.g, fg.b)
					result.b = bm_softlight(bg.b, fg.b)
				10: # Difference
					result.r = bm_difference(bg.r, fg.r)
					result.g = bm_difference(bg.g, fg.b)
					result.b = bm_difference(bg.b, fg.b)
				11: # Exclusion
					result.r = bm_exclusion(bg.r, fg.r)
					result.g = bm_exclusion(bg.g, fg.b)
					result.b = bm_exclusion(bg.b, fg.b)
				12: # Hue
					var blend = bm_setlum(bm_setsat(fg, bm_sat(bg)), bm_lum(bg))
					result.r = blend.r
					result.g = blend.g
					result.b = blend.b
				13: # Saturation
					var blend = bm_setlum(bm_setsat(bg, bm_sat(fg)), bm_lum(bg))
					result.r = blend.r
					result.g = blend.g
					result.b = blend.b
				14: # Color
					var blend = bm_setlum(fg,bm_lum(bg))
					result.r = blend.r
					result.g = blend.g
					result.b = blend.b
				15: # Luminosity
					var blend = bm_setlum(bg,bm_lum(fg))
					result.r = blend.r
					result.g = blend.g
					result.b = blend.b
				16: # Addition
					result.r = bm_addition(bg.r, fg.r)
					result.g = bm_addition(bg.g, fg.b)
					result.b = bm_addition(bg.b, fg.b)
				17: # Subtract
					result.r = bm_subtract(bg.r, fg.r)
					result.g = bm_subtract(bg.g, fg.b)
					result.b = bm_subtract(bg.b, fg.b)
				18: # Divide
					result.r = bm_divide(bg.r, fg.r)
					result.g = bm_divide(bg.g, fg.b)
					result.b = bm_divide(bg.b, fg.b)
			dst.set_pixel(rect.position.x + x, rect.position.y + y, result)
	src.unlock()
	dst.unlock()
	return dst.data.data

# Blend mode functions

func bm_multiply(a,b) -> float:
	return a * b

func bm_screen(a,b) -> float:
	return a - a * b + b

func bm_overlay(a,b) -> float:
	return b + (2 * a - 1) - (2 * a - 1) * b if a > 0.5 else a * b * 2

func bm_darken(a,b) -> float:
	return min(a, b)

func bm_lighten(a,b) -> float:
	return max(a, b)

func bm_colordodge(a,b) -> float:
	return 0.0 if a==0 else 1.0 if a>=1 - b else a / (1 - b)

func bm_colorburn(a,b) -> float:
	return 1.0 if a==1 else 0.0 if 1 - a >= b else 1 - (1 - a) / b

func bm_softlight(a,b) -> float:
	var d = sqrt(a) if a>0.25 else ((16 * a - 12) * a + 4) * a
	return a + (2 * b - 1) * (d - a) if b>0.5 else a - (1 - 2 * b) * a * (1 - a)

func bm_hardlight(a,b) -> float:
	return a + (2 * b - 1) - (2 * b - 1) * a if b>0.5 else a * b * 2

func bm_difference(a,b) -> float:
	return abs(a - b)

func bm_exclusion(a,b) -> float:
	return a + b - 2 * a * b

func bm_addition(a,b) -> float:
	return a + b

func bm_subtract(a,b) -> float:
	return a - b

func bm_divide(a,b) -> float:
	return 0.0 if a==0 else 1.0 if a>=b else a / b

# Helper functions for non-separable blend modes

func bm_lum(c) -> float:
	return (0.3 * c.r + 0.59 * c.g + 0.11 * c.b)

func bm_sat(c) -> float:
	return max(c.r, max(c.g, c.b)) - min(c.r, min(c.g, c.b))

func bm_setlum(c, d) -> Color:
	d = d - (0.3 * c.r + 0.59 * c.g + 0.11 * c.b)
	var dc = Vector3(c.r + d, c.g + d, c.b + d)
	var l = dc.dot(Vector3(0.3, 0.59, 0.11))
	var mm = Vector2(min(dc.x, min(dc.y, dc.z)), max(dc.x, max(dc.y, dc.z)))
	var lll = Vector3(l,l,l)
	if mm.x < 0 : dc = lll + (((dc - lll) * l) / (l - mm.x))
	if mm.y > 1: dc = lll + (((dc - lll) * (1-l)) / (mm.y - l))
	return Color(dc.x, dc.y, dc.z)

func bm_setsat(c, s) -> Color:
	# TODO: there has to be a better way
	var sort = [] # greatest value first
	sort = [(0 if c.r>=c.g and c.r>=c.b else 1 if c.g>=c.b else 2)]
	var og = [0,1,2]
	og.remove(sort[0])
	sort += og if og[0]>og[1] else [og[1], og[0]]
	if c[sort[0]]>c[sort[2]]:
		c[sort[1]] = ((c[sort[1]] - c[sort[2]])*s) / (c[sort[0]] - c[sort[2]])
		c[sort[2]] = 0
		c[sort[0]] = s
	else:
		c = ColorN('black')
	return c

# small utility functions

func scroll(file, steps):
	file.seek(file.get_position() + steps)

func get_str(file) -> String:
	var namelength = file.get_16()
	var namebytes = file.get_buffer(namelength).get_string_from_utf8()
	return namebytes

# file.get_##() returns a signed integer value
# except it doesn't. so we ensure this ourselves
func get_signed(integer)-> int:
	return integer - 4294967296 if bool(integer >= 2147483648) else integer

# stex function mostly taken from linked code
# https://github.com/lifelike/godot-animator-import/blob/master/addons/aaimport/aa_import_plugin.gd
# Copyright (c) 2018-2019 Pelle Nilsson
# Licensed under MIT (https://github.com/lifelike/godot-animator-import/blob/master/LICENSE.txt)
# TODO do i need to insert the full MIT license?
func save_stex(image, save_path):
  var tmppng = "%s-tmp.png" % [save_path]
  image.save_png(tmppng)
  var pngf = File.new()
  pngf.open(tmppng, File.READ)
  var pnglen = pngf.get_len()
  var pngdata = pngf.get_buffer(pnglen)
  pngf.close()
  Directory.new().remove(tmppng)
  var stexf = File.new()
  stexf.open("%s.stex" % [save_path], File.WRITE)
  stexf.store_32(0x54534447) # 'GDST'
  stexf.store_32(image.get_width())
  stexf.store_32(image.get_height())
  stexf.store_32(0x00000000) # flags
  stexf.store_32(0x07100000) # data format
  stexf.store_32(1) # nr mipmaps
  stexf.store_32(pnglen + 6)
  stexf.store_32(0x20474e50) # 'PNG '
  stexf.store_buffer(pngdata)
  stexf.close()
