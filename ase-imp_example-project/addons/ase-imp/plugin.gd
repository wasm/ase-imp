tool
extends EditorPlugin

var import_plugin_spr = null
var import_plugin_tex = null

func _enter_tree():
	import_plugin_spr = preload("import_ase_spr.gd").new()
	add_import_plugin(import_plugin_spr)
	import_plugin_tex = preload("import_ase_tex.gd").new()
	add_import_plugin(import_plugin_tex)

func _exit_tree():
	remove_import_plugin(import_plugin_spr)
	import_plugin_spr = null
	remove_import_plugin(import_plugin_tex)
	import_plugin_tex = null
