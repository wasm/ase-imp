tool
extends "import_ase.gd"

enum presets { DEFAULT }

func get_importer_name():
	return "ase.inc.spr"

func get_visible_name():
	return "Aseprite"

func get_recognized_extensions():
	return ["ase","aseprite"]

func get_save_extension():
	return "tscn"

func get_resource_type():
	return "PackedScene"

func get_preset_count():
	return presets.size()

func get_preset_name(preset):
	match preset:
		presets.DEFAULT:
			return "Default"
		_:
			return "Unknown"

func get_import_options(preset):
	match preset:
		presets.DEFAULT:
			return [
#				{"name": "import_as", "default_value": "Disabled,VRAM Compressed,Basis Universal", "property_hint": PROPERTY_HINT_ENUM},
				{"name": "sprite/animations", "default_value": true},
				{"name": "sprite/colliders", "default_value": false},
				{"name": "sprite/rename", "default_value": false},
				{"name": "sprite/name", "default_value": 'test'},
				{"name": "layout/layout", "default_value": 'test,test'}, # enum: horizontal strip, vertical strip, rectangle
				{"name": "flags/repeat", "default_value": 0}, # enum: disabled, enabled, mirrored
				{"name": "flags/filter", "default_value": false},
			]
		_: return []

func get_option_visibility(option, options):
	if option == "sprite/name" && !bool(options["sprite/rename"]):
		return false
	else:
		return true

func import(source_file, save_path, options, r_platform_variants, r_gen_files):
	var ase = read_ase(source_file)

	var asset = PackedScene.new()
	var sprite = Sprite.new()
	# set to filename unless there's a user-provided override
	sprite.set_name(source_file.get_file().rsplit('.',true,1)[0])
	build_animator(ase, sprite)
	
	var img = build_atlas(ase)
	var imgtex = ImageTexture.new()
	imgtex.create_from_image(img)
	imgtex.set_flags(0)
	sprite.texture = imgtex
	sprite.hframes = imgtex.get_width() / ase.header.width
	sprite.vframes = imgtex.get_height() / ase.header.height
	# endif
		
	var result = asset.pack(sprite)
	return ResourceSaver.save("%s.%s" % [save_path, get_save_extension()], asset)

