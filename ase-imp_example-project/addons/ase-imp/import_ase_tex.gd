tool
extends "import_ase.gd"

func get_importer_name():
	return "ase.inc.tex"

func get_visible_name():
	return "Aseprite Texture"

func get_recognized_extensions():
	return ["ase","aseprite"]

func get_save_extension():
	return "stex"

func get_resource_type():
	return "StreamTexture"

func get_preset_count():
	return 1

func get_preset_name(preset):
	return "Default"

func get_import_options(preset):
	return []

func get_option_visibility(option, options):
	return true

func get_priority():
	return 1.1

func import(source_file, save_path, options, r_platform_variants, r_gen_files):
	var ase = read_ase(source_file)

	var img = build_atlas(ase)
	save_stex(img, save_path)
	return OK
